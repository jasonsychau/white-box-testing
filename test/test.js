const expect = require("chai").expect;
const {Builder, By, Key, until} = require('selenium-webdriver');

describe("testing https://www.neptuneubc.com/", function() {
	this.timeout(600000);
	
	var driver = null;
	beforeEach(function() {
		return new Builder().forBrowser('firefox').build().then(async res=>{
			driver = res;
			try {
				await driver.get('https://www.neptuneubc.com/');
			} finally {
				return null;
			}
		});
	});

	afterEach(function() {
		return driver.quit().then(function(){ return null; });
	});

	it("test reset page position after selecting category", async function () {
		return driver.wait(until.elementLocated(By.id('j1_10_anchor')), 10000).then(async ()=>{
			const rootElement = await driver.findElement(By.xpath("/*"));

			await driver.executeScript("window.scrollBy(0,100)", rootElement);

			const scrollVal = await rootElement.getAttribute("scrollTop")
			expect(scrollVal).to.equal("100");

			const categoryLnk = await driver.findElement(By.id("j1_10_anchor"));
			await categoryLnk.click();

			const newScrollVal = await rootElement.getAttribute("scrollTop");
			expect(newScrollVal).to.equal("0");

			return null;
		});

	});
});
